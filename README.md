# Houdini alembic multiloader

This simple widget lets the user choose multiple .abc files from anywhere in the computer and add them to different
geo nodes (here called groups), to be batch-created. The names for the geo nodes can also be customized before creation.

### Recommended way to launch
Install the module to your desired location and run:
```
from houdini_abc_multiloader import loader_ui
l = loader_ui.MultiImporter()
```

### Look of the widget
![abc_loader](http://www.jaimervq.com/wp-content/uploads/2020/01/abc_multi_loader.jpg)

This module has been tested successfully in **Houdini 17.5.298**
***

For more info: www.jaimervq.com